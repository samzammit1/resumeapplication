//
//  experienceController.swift
//  resumeApplication
//
//  Created by Sam Zammit on 8/12/19.
//  Copyright © 2019 Sam Zammit. All rights reserved.
//

import UIKit

struct Headline {
    var id : Int
    var title : String
    var text : String
    var image : String
    var section : Int
}

class experienceController: UITableViewController {

    var headlines = [
        Headline(id: 1, title: "OmniChannel Media", text: "Nov 2017 - Jan 2018", image: "Omni", section: 0),
        Headline(id: 2, title: "James Bennett LTD", text: "May 2015 - Nov 2017", image: "JB", section: 0),
        Headline(id: 3, title: "Balgowlah RSL", text: "Sep 2017 - Current", image: "RSL", section: 0),
        Headline(id: 4, title: "Bachelor of Business (Completed)", text: "Majoring in Extended Marketing", image: "UTS", section: 1),
        Headline(id: 5, title: "Bachelor of Software Engineering", text: "Due for Graduation in 2021", image: "UTS", section: 1),

    ]
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return headlines.count
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section == 0){
            return "Work Experience"
        }else if(section == 1){
            return "Education"
        }
            return "Section not found."
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "omniCell", for: indexPath)

        let headline = headlines[indexPath.row]
        cell.textLabel?.text = headline.title
        cell.detailTextLabel?.text = headline.text
        cell.imageView?.image = UIImage(named: headline.image)

        return cell
    }

}
