//
//  skillsControllerViewController.swift
//  resumeApplication
//
//  Created by Sam Zammit on 8/12/19.
//  Copyright © 2019 Sam Zammit. All rights reserved.
//

import UIKit

class skillsController: UIViewController {

    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var yConstraint: NSLayoutConstraint!

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func whenPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 3, delay: 1, options: [.curveEaseInOut, .preferredFramesPerSecond60], animations: {
            self.animationView.superview?.layoutIfNeeded()
            self.animationView.backgroundColor = UIColor.red
        }) { (completed) in
            if completed {
                self.advancedAnimation()
            } else {
                print("Animation was cancelled")
            }
            
        }
    }
    
    func advancedAnimation() {
           let startPosition = animationView.center
           UIView.animateKeyframes(withDuration: 4, delay: 0.5, options: [.repeat, .autoreverse], animations: {
               UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1) {
                   self.animationView.backgroundColor = UIColor.blue
               }
               
               UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.2) {
                   self.animationView.center = self.view.center
               }
               
               UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.3) {
                   self.animationView.transform = CGAffineTransform(rotationAngle: CGFloat.pi).concatenating(CGAffineTransform(scaleX: 2, y: 2))
               }
               
               UIView.addKeyframe(withRelativeStartTime: 0.6, relativeDuration: 0.4) {
                   self.animationView.center = startPosition
                   self.animationView.transform = CGAffineTransform.identity
               }
           }) { (completed) in
               print("Completed: \(completed)")
           }
       }
    
}
